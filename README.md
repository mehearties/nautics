Nautics
=========

Tests: ![tests](https://gitlab.com/mehearties/nautics/badges/master/pipeline.svg) ![coverage](https://gitlab.com/mehearties/nautics/badges/master/coverage.svg)

Nautics is a collection of utilities for players of Puzzle Pirates.

Tools
-----

### Cursed Isles Forage Counter

The purpose of the Cursed Isles Forage Counter (CIFC) is to allow the user to generate a score for each player on a seven-man sloop Cursed Isles Sea Monster Hunt, at the end of each foraging session.

#### Usage

The current version will provide functionality for players using an 800x600 resolution game window. The user will need to press F12 or type /print in the Puzzle Pirates game window when the duty report appears at the end of a forage, making sure that they have scrolled to the bottom of the scroll box, to take a screenshot and ensure their screenshot directory matches the directory expected by CIFC.

Once the screenshot has been taken, CIFC can be run which will extract each plater's name and score. A copy of the screenshot will be saved into a historical data folder together with a text file containing the names and scores. The names and scores will be copied to the clipboard, allowing them to be pasted into the Puzzle Pirates client to announce to the crew.

#### Future Plans

Future plans include:
* adding ability to track forage sessions accross a CI entry to give players' average scores per dip; and
* adding better functionality to store data gathered on runs and analyise it with a view to gathering information on pirates such as:
    1. lifetime average score;
    2. average score over last 20 CIs; and
    3. highest score in a single forage.

Installation
------------

You need the following additional dependencies:

Python 3.5 or higher
Tesseract OCR (https://github.com/tesseract-ocr/tesseract/wiki)


Tests
-----

Install dependencies from the 'Installation' section. Clone the repository, then:

```
pip install -r requirements.txt
pip install -r requirements_tests.txt
python -m unittest -v
```

Copyright
---------

The work in this project is:

    Copyright (C) 2018 David Wiltshire
    Copyright (C) 2018 Jonathan Wiltshire
