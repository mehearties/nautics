# import the necessary packages
from PIL import Image
import pytesseract
import numpy as np
import argparse
import cv2
import os
import time
import datetime
from pyperclip import copy as clipcopy
from tempfile import TemporaryDirectory
import logging as log

templates_dir = os.path.join(os.path.abspath(os.path.dirname(__file__)), 'forage_templates')

players = (
    ((200, 109, 293, 126), (200, 146, 287, 167)),
    ((200, 167, 293, 184), (193, 205, 287, 227)),
    ((200, 225, 293, 243), (193, 265, 287, 284)),
    ((200, 284, 293, 302), (193, 322, 287, 342)),
    ((200, 342, 293, 359), (193, 379, 287, 400)),
    ((200, 399, 293, 417), (193, 438, 287, 457)),
    ((200, 457, 293, 474), (193, 495, 287, 516))
)


def __find_tesseract():
    possible_paths = (
        # unix-y ones:
        os.path.join('/', 'usr', 'bin', 'tesseract'),
        # windows-y ones:
        os.path.join(os.path.expanduser('~'), 'AppData', 'Local', 'Tesseract-OCR', 'tesseract.exe'),
        # TODO: add others
    )

    for path in possible_paths:
        log.debug("Trying {0}".format(path))
        if os.path.isfile(path):
            log.debug("Found tesseract at {0}".format(path))
            return path

    # none found:
    log.debug("No tesseract found, relying on defaults")
    return None


def cropDutyReport(screenshot):
    # Extract duty report from any available YPP client resolution
    resolutions = {
        '800x600': (0, 0, 449, 599),
        '1366x768':(283, 84, 732, 683),
        '1024x768':(112, 84, 561, 683),
        '1152x855':(176, 127, 625, 726),
        '1280x600':(240, 0, 689, 599),
        '1280x768':(240, 84, 689, 683),
        '1280x855':(240, 127, 689, 726),
        '1024x640':(112, 20, 561, 619),
        '1360x768':(280, 84, 729, 683),
        '1400x855':(300, 127, 749,726),
        '1440x855':(320, 127, 769, 726),
        '1550x855':(375, 127, 824, 726),
    }
    width, height = screenshot.size
    reskey = str(width)+'x'+str(height)
    log.debug("detected resolution: {0}".format(reskey))
    try:
        return screenshot.crop(resolutions[reskey])
    except:
        print("The screenshot produced by your Puzzle Pirates client is", reskey + ".",
              "The following resolutions are currently supported:" + "\n", '800x600' + '\n',
              '1024x640' + '\n', '1024x768' + '\n', '1152x864' + '\n', '1280x600' + '\n',
              '1280x768' + '\n', '1280x855' + '\n', '1360x768' + '\n', '1366x768' + '\n',
              '1400x855' + '\n', '1440x855' + '\n', '1550x855')


def get_name(screenshot, box):
    # Extract player name box
    cropped_image = screenshot.crop(box)
    with TemporaryDirectory() as imgdir:
        imgfile = os.path.join(imgdir, 'player.png')
        cropped_image.save(imgfile)
        player_string = get_player_string(imgfile)

    return player_string

    
def get_score(screenshot, box):
    # extract forage box
    cropped_image = screenshot.crop(box)

    with TemporaryDirectory() as imgdir:
        imgfile = os.path.join(imgdir, 'score.png')
        cropped_image.save(imgfile)
        score = BBCounter(imgfile) + (JarCounter(imgfile) * 2) + (CCCounter(imgfile) * 3)

    return score
    
def get_player_string(image_file):
    # Read image using opencv
    img = cv2.imread(image_file)

    # Rescale the image, if needed.
    img = cv2.resize(img, None, fx=3.5, fy=3.5, interpolation=cv2.INTER_CUBIC)

    # Convert to gray
    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # Apply dilation and erosion to remove some noise
    kernel = np.ones((1, 1), np.uint8)
    img = cv2.dilate(img, kernel, iterations=1)
    img = cv2.erode(img, kernel, iterations=1)
    # Apply blur to smooth out the edges
    img = cv2.GaussianBlur(img, (5, 5), 0)

    # Apply threshold to get image with only b&w (binarization)
    img = cv2.threshold(img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

    # Recognize text with tesseract for python
    return pytesseract.image_to_string(img)


def BBCounter(score_image):
    img_rgb = cv2.imread(score_image)
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
    count = 0

    template = cv2.imread(os.path.join(templates_dir, 'BBPixels.png'), 0)
    w, h = template.shape[::-1]

    res= cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
    threshold = 0.90
    loc = np.where( res >= threshold)

    for pt in zip(*loc[::-1]):
                  cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0, 255, 255), 2)
                  count = count + 1

    return(count)
    
def JarCounter(score_image):
    img_rgb = cv2.imread(score_image)
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
    count = 0 

    template = cv2.imread(os.path.join(templates_dir, 'JarPixels.png'), 0)
    w, h = template.shape[::-1]

    res= cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
    threshold = 0.99
    loc = np.where( res >= threshold)

    for pt in zip(*loc[::-1]):
                  cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0, 255, 255), 2)
                  count = count + 1

    return(count)

def CCCounter(score_image):
    img_rgb = cv2.imread(score_image)
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_BGR2GRAY)
    count = 0

    template = cv2.imread(os.path.join(templates_dir, 'CCPixels.png'), 0)
    w, h = template.shape[::-1]

    res = cv2.matchTemplate(img_gray, template, cv2.TM_CCOEFF_NORMED)
    threshold = 0.80
    loc = np.where( res >= threshold)

    for pt in zip(*loc[::-1]):
                  cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0, 255, 255), 2)
                  count = count + 1
                  
    return(count)


def main(screenshot_path=None, output_dir=None, delete_after=True):

    myhome = os.path.expanduser('~')
    log.debug("myhome: {0}".format(myhome))

    pytesseract.pytesseract.tesseract_cmd = __find_tesseract()

    #Set working directory
    if not output_dir:
        output_dir = os.path.join(myhome, 'Documents', 'OCR')
    log.debug("output_dir: {0}".format(output_dir))

    #set directory that screenshots and scores will be saved into once processed for future reference 
    historical_dir = os.path.join(output_dir, 'Historical Screenshots and Data')
    log.debug("historical_dir: {0}".format(historical_dir))

    # Get the time and date which will be used for naming the historical screenshots and scores
    ds = datetime.datetime.fromtimestamp(time.time()).strftime('%d-%m-%y %H-%M-%S')
    log.debug("ds: {0}".format(ds))

    #Create a text file which will hold the scores so they can be copied to the clipboard
    #and kept for future reference. File name will be the datestamp which was just created
    #f = open(historical_dir + ds + '.txt', 'a+')
    with open(os.path.join(historical_dir, ds + '.txt'), 'a+') as f:
        #Read the screenshot using Pillow
        if not screenshot_path:
            screenshot_path = os.path.join(output_dir, 'Puzzle Pirates Uino 1.png')
        log.debug("screenshot_path: {0}".format(screenshot_path))

        screenshot = Image.open(screenshot_path)

        #Save a copy of the screenshot to the historical data folder
        screenshot.save(os.path.join(historical_dir, ds + '.png'))

        #Crop screenshot from any resolution to leave only the forage duty report
        duty_report_screenshot = cropDutyReport(screenshot)

        #Run through each player
        for player in players:
            log.debug("finding player information at {0}".format(player))
            player_name = get_name(duty_report_screenshot, player[0])
            player_score = get_score(duty_report_screenshot, player[1])
            log.info("{player} scores {score}".format(player=player_name, score=player_score))
            if player_score > 0:
                f.write("{player} {score}\n".format(player=player_name, score=player_score))

            # Copy to clipboard
            f.seek(0)
            data = f.read()

    #Delete original screenshot so that next screenshot is named correctly
    if delete_after:
        log.debug("deleting screenshot")
        os.remove(screenshot_path)

    return(data)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Identify scores of players after a forage session")
    parser.add_argument('--screenshot', '-s', help='screenshot to process')
    parser.add_argument('--output', '-o', help='output directory')
    parser.add_argument('--no-delete', '-n', action='store_false',
                        help='do not delete original screenshot after processing')
    args = parser.parse_args()

    log.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

    output = main(screenshot_path=args.screenshot, output_dir=args.output, delete_after=args.no_delete)
    clipcopy(output)
