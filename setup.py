from setuptools import setup

setup(
    name='nautics',
    version='0.1-dev0',
    packages=[
        'nautics',
        'tests',
    ],
    package_data={
        'nautics': ['forage_templates/*.png'],
    },
    url='https://gitlab.com/mehearties/nautics',
    description='Collection of utilities for players of Puzzle Pirates',
    install_requires=[
        'numpy',
        'opencv_python',
        'Pillow',
        'pyperclip',
        'pytesseract',
    ],
    tests_require=[
        'coverage',
        'parameterized',
    ],
)
