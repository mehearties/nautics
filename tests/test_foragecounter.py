from parameterized import parameterized
import os
import unittest
from tempfile import TemporaryDirectory
from nautics import foragecounter
from PIL import Image
from io import StringIO
import sys


class ForageCounterTests(unittest.TestCase):
    @parameterized.expand([
        ('1.png', '1.txt'),
        ('2.png', '2.txt'),
        ('3.png', '3.txt'),
        ('4.png', '4.txt'),
        ('5.png', '5.txt'),
        ('6.png', '6.txt'),
        ('6man1.png', '6man1.txt'),
    ])
    def test_sample_screenshots(self, sample, expected):
        with TemporaryDirectory() as tempdir:
            os.mkdir(os.path.join(tempdir, 'Historical Screenshots and Data'))
            with open(os.path.join('tests', 'samples', expected)) as expected_output:
                output = foragecounter.main(os.path.join('tests', 'samples', sample), tempdir, False)
                self.assertEqual(output, expected_output.read())

    @parameterized.expand([
        '800x600',
        '1024x640',
        '1024x768',
        '1152x864',
        '1280x600',
        '1280x768',
        '1280x855',  # actual output of 1600x1200
        '1360x768',
        '1366x768',
        '1400x855',  # actual output of 1400x1050
        '1440x855',  # actual output of 1440x900
        '1550x855',  # actual output of 1600x900
    ])
    def test_crop_duty_report(self, resolution):
        with Image.open(os.path.join('tests', 'resolution_tests', resolution+'.png')) as testcard:
            expected_size = (449, 599)
            self.assertEqual(expected_size, foragecounter.cropDutyReport(testcard).size)

    @parameterized.expand([
        ('800x600.png', '800x600.txt'),
        ('1024x640.png', '1024x640.txt'),
        ('1024x768.png', '1024x768.txt'),
        ('1152x864.png', '1152x864.txt'),
        ('1280x768.png', '1280x768.txt'),
        ('1280x600.png', '1280x600.txt'),
        ('1280x855.png', '1280x855.txt'),
        ('1360x768.png', '1360x768.txt'),
        ('1366x768.png', '1366x768.txt'),
        ('1400x855.png', '1400x855.txt'),
        ('1440x855.png', '1440x855.txt'),
        ('1550x855.png', '1550x855.txt'),
    ])
    def test_cropped_screenshots(self, sample, expected):
        # Verify that output of cropped images from different resolutions matches expected output
        with TemporaryDirectory() as tempdir:
            os.mkdir(os.path.join(tempdir, 'Historical Screenshots and Data'))
            with open(os.path.join('tests', 'resolution_tests', expected)) as expected_output:
                output = foragecounter.main(os.path.join('tests', 'resolution_tests', sample), tempdir, False)
                self.assertEqual(output, expected_output.read())

    def test_unsupported_resolution(self):
        # Verify that an unsupported resolution screenshot aborts appropriately
        captured_output = StringIO()
        sys.stdout = captured_output
        img = Image.new('RGB', (10, 10))
        self.assertIsNone(foragecounter.cropDutyReport(img), None)
        self.assertIn('The screenshot produced by your Puzzle Pirates client is 10x10.', captured_output.getvalue())

    @parameterized.expand([
        ("chests1.png", 6),
        ("chests2.png", 7),
        ("chests3.png", 5),
        ("chests4.png", 7),
        ("chests5.png", 5),
        ("face.png", 0),
    ])
    def test_bb_counter(self, sample, expected):
        # Verify that output of the BBCounter function is accurate
        output = foragecounter.BBCounter(os.path.join('tests', 'counter_component_tests', sample))
        self.assertEqual(output, expected)

    @parameterized.expand([
        ("chests1.png", 4),
        ("chests2.png", 5),
        ("chests3.png", 4),
        ("chests4.png", 5),
        ("chests5.png", 9),
        ("face.png", 0),
    ])
    def test_jar_counter(self, sample, expected):
        # Verify that output of the JarCounter function is accurate
        output = foragecounter.JarCounter(os.path.join('tests', 'counter_component_tests', sample))
        self.assertEqual(output, expected)

    @parameterized.expand([
        ("chests1.png", 1),
        ("chests2.png", 4),
        ("chests3.png", 2),
        ("chests4.png", 1),
        ("chests5.png", 0),
        ("face.png", 0),
    ])
    def test_cc_counter(self, sample, expected):
        # Verify that output of the CCCounter function is accurate
        output = foragecounter.CCCounter(os.path.join('tests', 'counter_component_tests', sample))
        self.assertEqual(output, expected)


